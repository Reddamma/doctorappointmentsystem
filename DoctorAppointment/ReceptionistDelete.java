package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ReceptionistDelete extends JFrame {

	private JPanel contentPane;
	String REC_NAME, REC_ID;
	private String Rec_Id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		ReceptionistDelete frame = new ReceptionistDelete();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public ReceptionistDelete() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 781, 531);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDeleteRecName = new JLabel("Delete Rec_Id");
		lblDeleteRecName.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblDeleteRecName.setBounds(274, 83, 216, 36);
		contentPane.add(lblDeleteRecName);

		JLabel lblEnterRecName = new JLabel("Enter Rec_Id");
		lblEnterRecName.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblEnterRecName.setBounds(112, 171, 255, 36);
		contentPane.add(lblEnterRecName);

		JComboBox cmbRecName = new JComboBox();
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			String str = "select * from Receptionist order by rec_name";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);
			while (rs.next()) {
				cmbRecName.addItem(rs.getString(1) + "    " + rs.getString(2));

			}
		} catch (Exception e) {

		}
		cmbRecName.setBounds(369, 181, 181, 22);
		contentPane.add(cmbRecName);

		JButton btnDelete = new JButton("Delete");
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnDelete.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				REC_NAME = cmbRecName.getSelectedItem().toString().substring(0, 3);
				int rec_Id = Integer.parseInt(REC_NAME);
				delete(rec_Id);

			}
		});

		btnDelete.setBounds(612, 288, 97, 36);
		contentPane.add(btnDelete);

		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage lp = new AdminPage();
				lp.setVisible(true);
				show(false);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnNewButton.setBounds(437, 288, 97, 36);
		contentPane.add(btnNewButton);

	}

	public void delete(int rec_Id) {
		Connection cn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			stmt = cn.createStatement();
			System.out.println("DELETE from Receptionist where Rec_Id= " + rec_Id);
			boolean i = stmt.execute("DELETE from Receptionist where Rec_Id= " + rec_Id);
			if (i == false) {
				JOptionPane.showMessageDialog(null, "Receptionist deleted sucessfully", "Welcome",
						JOptionPane.PLAIN_MESSAGE);
				show(true);
			} else {
				JOptionPane.showMessageDialog(null, "Receptionist Not deleted Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {

			e.printStackTrace();
		}

	}

}
