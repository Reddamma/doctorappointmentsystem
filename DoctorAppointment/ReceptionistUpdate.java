package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class ReceptionistUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField txtrecid;
	private JTextField txtrecname;
	private JTextField txtphoneno;
	String C_NAME, PH_NO, C_ID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		ReceptionistUpdate frame = new ReceptionistUpdate();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public ReceptionistUpdate() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 739, 536);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUpdateeceptionist = new JLabel("Update Receptionist");
		lblUpdateeceptionist.setFont(new Font("Sitka Subheading", Font.BOLD, 30));
		lblUpdateeceptionist.setBounds(182, 11, 379, 27);
		contentPane.add(lblUpdateeceptionist);

		JLabel lblRecId = new JLabel("Rec_ID");
		lblRecId.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblRecId.setBounds(38, 90, 165, 27);
		contentPane.add(lblRecId);

		txtrecid = new JTextField();
		txtrecid.setBounds(257, 85, 223, 36);
		contentPane.add(txtrecid);
		txtrecid.setColumns(10);

		JLabel lblPhoneNo = new JLabel("Phone_No");
		lblPhoneNo.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblPhoneNo.setBounds(38, 217, 136, 20);
		contentPane.add(lblPhoneNo);

		txtphoneno = new JTextField();
		txtphoneno.setBounds(257, 216, 223, 36);
		contentPane.add(txtphoneno);
		txtphoneno.setColumns(10);

		JLabel lblrecName = new JLabel("Rec_Name");
		lblrecName.setFont(new Font("Sitka Subheading", Font.BOLD, 18));
		lblrecName.setBounds(38, 153, 136, 20);
		contentPane.add(lblrecName);

		txtrecname = new JTextField();
		txtrecname.setBounds(257, 152, 223, 36);
		contentPane.add(txtrecname);
		txtrecname.setColumns(10);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection cn = null;
				PreparedStatement stmt = null;
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");

					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
					PreparedStatement ps = cn
							.prepareStatement("UPDATE Receptionist set Rec_Name=?,Phone_NO=? where  Rec_Id=?");
					ps.setString(1, txtrecname.getText());
					ps.setString(2, txtphoneno.getText());
					ps.setString(3, txtrecid.getText());
					int i = ps.executeUpdate();
					if (i != 0) {
						JOptionPane.showMessageDialog(null, "Receptionist Updated sucessfully", "Welcome",
								JOptionPane.PLAIN_MESSAGE);

						show(true);
					} else {
						JOptionPane.showMessageDialog(null, "Receptionist Not Updated Successfully", "Error",
								JOptionPane.ERROR_MESSAGE);

					}
				} catch (ClassNotFoundException e) {

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnUpdate.setBounds(487, 328, 136, 42);
		contentPane.add(btnUpdate);
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage Ap = new AdminPage();
				Ap.setVisible(true);
				show(false);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnBack.setBounds(308, 331, 125, 36);
		contentPane.add(btnBack);
	}
}