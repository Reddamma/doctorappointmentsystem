package DoctorAppointment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.Collections;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ViewPatient extends JFrame {

	DefaultTableModel model = new DefaultTableModel();
	Container cnt = this.getContentPane();
	JTable jtbl = new JTable(model);
	private Container contentPane;
	JTextField txtSear = new JTextField();

	public ViewPatient() {
		cnt.setLayout(new FlowLayout(FlowLayout.LEFT));
		model.addColumn("PATIENT_ID");
		model.addColumn("PHONE_NO");
		model.addColumn("PATIENT_NAME");
		
		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReceptionistPage lp = new ReceptionistPage();
				lp.setVisible(true);
				show(false);

			}
		});
		btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
		btnBack.setBounds(335, 194, 89, 22);
		cnt.add(btnBack);

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			PreparedStatement pstm = cn.prepareStatement("SELECT * from Patient");
			ResultSet Rs = pstm.executeQuery();
			while (Rs.next()) {
				model.addRow(new Object[] { Rs.getInt(1),Rs.getLong(2),Rs.getString(3)});
			}
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}

		JButton btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			private String PATIENT_ID;
			private String PHONE_NO;
			private String PATIENT_NAME;

			public void actionPerformed(ActionEvent arg0) {
				try {
					System.out.println("in search");
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
					String str = txtSear.getText().trim();
					System.out.println(str);
					java.sql.Statement st = cn.createStatement();
					String query = "select * from Patient where PATIENT_NAME like '%" + str + "%'";

					System.out.println(query);

					ResultSet Rs = st.executeQuery(query);
					while (Rs.next() == true) {
						System.out.println(Rs.getString(3));
						model.setRowCount(0);

						model.addRow(new Object[] { Rs.getInt(PATIENT_ID),Rs.getLong(PHONE_NO),Rs.getString(PATIENT_NAME) });
					}

				} catch (Exception e) {

				}

			}
		});
		JLabel lblPatientname = new JLabel("PatientName");
		lblPatientname.setFont(new Font("Tahoma", Font.PLAIN, 18));
		getContentPane().add(lblPatientname);

		getContentPane().add(txtSear);
		txtSear.setColumns(10);
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 20));
		getContentPane().add(btnSearch);

		JScrollPane pg = new JScrollPane(jtbl);
		cnt.add(pg);

		this.pack();
	}

	public static void main(String[] args) {
		JFrame fr = new ViewPatient();
		fr.setTitle("PatientDetails");
		fr.setSize(500, 400);
		fr.setLocationRelativeTo(null);
		fr.setVisible(true);
		fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
