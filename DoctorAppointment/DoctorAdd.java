package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class DoctorAdd extends JFrame {

	private JPanel txtTimeTo;
	private JTextField txtName;
	private JTextField txtPhn;
	private JTextField txtSpec;
	private JTextField txtTimeAvlFrom;
	private JTextField txtTimeAvlTo;
	String DOC_NAME, PHONE_NO, SPEC_ID, PASSWORD, DAY_AVL_FROM, DAY_AVL_TO, TIME_AVL_FROM, TIME_AVL_TO;
	protected JComboBox cmbAvlFrom;
	protected JComboBox cmbAvlTo;
	private JPasswordField Pswd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		DoctorAdd frame = new DoctorAdd();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public DoctorAdd() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 716, 509);
		txtTimeTo = new JPanel();
		txtTimeTo.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(txtTimeTo);
		txtTimeTo.setLayout(null);

		JLabel lblNewLabel = new JLabel("Doc_Name");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(28, 75, 121, 16);
		txtTimeTo.add(lblNewLabel);

		JLabel lblNewLabel_2 = new JLabel("password");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_2.setBounds(28, 171, 121, 25);
		txtTimeTo.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("phone_No");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_3.setBounds(28, 104, 121, 28);
		txtTimeTo.add(lblNewLabel_3);

		JLabel lblNewLabel_4 = new JLabel("Spec_ID");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_4.setBounds(28, 137, 121, 25);
		txtTimeTo.add(lblNewLabel_4);

		txtName = new JTextField();
		txtName.setBounds(183, 72, 116, 22);
		txtTimeTo.add(txtName);
		txtName.setColumns(10);

		txtPhn = new JTextField();
		txtPhn.setBounds(183, 110, 116, 22);
		txtTimeTo.add(txtPhn);
		txtPhn.setColumns(10);

		txtSpec = new JTextField();
		txtSpec.setBounds(183, 139, 116, 22);
		txtTimeTo.add(txtSpec);
		txtSpec.setColumns(10);

		JLabel lblDayavlfrom = new JLabel("Day_avl_From");
		lblDayavlfrom.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDayavlfrom.setBounds(27, 209, 122, 22);
		txtTimeTo.add(lblDayavlfrom);

		JLabel lblDayavlto = new JLabel("Day_avl_To");
		lblDayavlto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblDayavlto.setBounds(28, 248, 121, 26);
		txtTimeTo.add(lblDayavlto);

		JLabel lblTimeavlfrom = new JLabel("Time_avl_From");
		lblTimeavlfrom.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTimeavlfrom.setBounds(28, 287, 121, 16);
		txtTimeTo.add(lblTimeavlfrom);

		txtTimeAvlFrom = new JTextField();
		txtTimeAvlFrom.setBounds(183, 284, 116, 22);
		txtTimeTo.add(txtTimeAvlFrom);
		txtTimeAvlFrom.setColumns(10);

		JLabel lblTimeavlto = new JLabel("Time_avl_To");
		lblTimeavlto.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTimeavlto.setBounds(28, 325, 121, 16);
		txtTimeTo.add(lblTimeavlto);

		txtTimeAvlTo = new JTextField();
		txtTimeAvlTo.setBounds(183, 322, 116, 22);
		txtTimeTo.add(txtTimeAvlTo);
		txtTimeAvlTo.setColumns(10);

		String AvlFrom[] = { "None", "Mon", "Tue", "wed", "Thur", "Fri", "Sat", "Sun" };
		JComboBox cmbAvlFrom = new JComboBox(AvlFrom);
		cmbAvlFrom.setBounds(183, 206, 116, 22);
		txtTimeTo.add(cmbAvlFrom);

		String AvlTo[] = { "None", "Mon", "Tue", "wed", "Thur", "Fri", "Sat", "Sun" };
		JComboBox cmbAvlTo = new JComboBox(AvlTo);
		cmbAvlTo.setBounds(183, 249, 116, 22);
		txtTimeTo.add(cmbAvlTo);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DOC_NAME = txtName.getText().trim();
				PHONE_NO = txtPhn.getText().trim();
				SPEC_ID = txtSpec.getText().trim();
				PASSWORD = Pswd.getText().trim();
				DAY_AVL_FROM = cmbAvlFrom.getSelectedItem().toString().trim();
				DAY_AVL_TO = cmbAvlTo.getSelectedItem().toString().trim();
				TIME_AVL_FROM = txtTimeAvlFrom.getText().trim();
				TIME_AVL_TO = txtTimeAvlTo.getText().trim();
				insert();

			}
		});
		btnAdd.setBounds(499, 376, 97, 25);
		txtTimeTo.add(btnAdd);

		JLabel lblAddDoctor = new JLabel("Add Doctor");
		lblAddDoctor.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAddDoctor.setBounds(253, 13, 121, 32);
		txtTimeTo.add(lblAddDoctor);

		Pswd = new JPasswordField();
		Pswd.setBounds(183, 168, 116, 22);
		txtTimeTo.add(Pswd);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage ap = new AdminPage();
				ap.setVisible(true);
				show(false);
			}
		});
		btnBack.setBounds(364, 376, 97, 25);
		txtTimeTo.add(btnBack);
	}

	public void insert() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");

			PreparedStatement st = cn.prepareStatement("insert into doctor values (doctorSeq.nextval,?,?,?,?,?,?,?,?)");
			st.setString(1, DOC_NAME);
			st.setString(2, PHONE_NO);
			st.setString(3, SPEC_ID);
			st.setString(4, PASSWORD);
			st.setString(5, DAY_AVL_FROM);
			st.setString(6, DAY_AVL_TO);
			st.setString(7, TIME_AVL_FROM);
			st.setString(8, TIME_AVL_TO);

			int i = st.executeUpdate();
			if (i != 0) {
				JOptionPane.showMessageDialog(null, "Doctor added sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
			} else
				JOptionPane.showMessageDialog(null, "Doctor Not added Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());

		} catch (NullPointerException e) {

			e.printStackTrace();
		}
	}
}