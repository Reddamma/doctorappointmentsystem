package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.*;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextPane;

import com.toedter.calendar.JDateChooser;
import java.util.Date;

public class BookAppointment extends JFrame {

	private JPanel contentPane;
	private JTextField txtSearch;
	private JTextField PatientName;
	private JTextField timeavlfrom;
	private JTextField timeavlto;
	private String App_Id;
	private String Patient_Id;
	private String Patient_Name;
	private String doc_Id;
	private String DAY_AVL_FROM;
	private String DAY_AVL_TO;
	private String TIME_AVL_FROM;
	private String TIME_AVL_TO;
	private String app_Time;

	private JComboBox comboBox;
	private JComboBox cmbspecialization;

	Date App_Date;
	//JDateChooser j;
	DefaultTableModel model = new DefaultTableModel();
	Container cnt = this.getContentPane();
	JTable jtbl = new JTable(model);
	private Container contentPane1;
	JTextField txtSearch1 = new JTextField();
	private JTextField btnSearch;
	private JTextField txtid;
	private JTextField Dayavlfrom;
	private JTextField Dayavlto;
	private ItemListener itemListener;
	String DAY_AVL_FROM1;
	String DAY_AVL_to1;
	String time_AVL_FROM1;
	String time_AVL_to1;
	// private Component appDate;
	// private JTextField AppDate;
	JDateChooser dateChooser;

	/// int App_Date;
	// Date App_Date;
	private String Specialization;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BookAppointment frame = new BookAppointment();
		frame.setVisible(true);

	}

	//JDateChooser dateChooser;

	/**
	 * Create the frame.
	 */
	public BookAppointment() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		model.addColumn("App_ID");
		model.addColumn("Patient_Id");
		model.addColumn("Patient_Name");
		model.addColumn("doc_id");
		model.addColumn("DAY_AVL_FROM");
		model.addColumn("DAY_AVL_TO ");
		model.addColumn("TIME_AVL_FROM");
		model.addColumn("TIME_AVL_TO");
		model.addColumn("App_Date");
		model.addColumn("App_Time");

		setBounds(100, 100, 885, 606);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		 dateChooser = new JDateChooser();

		JLabel lblBookingAppointment = new JLabel("Booking Appointment");
		lblBookingAppointment.setFont(new Font("Sitka Subheading", Font.BOLD, 25));
		lblBookingAppointment.setBounds(210, 13, 259, 31);
		contentPane.add(lblBookingAppointment);

		JLabel lblPatientid = new JLabel("Patient_Id");
		lblPatientid.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblPatientid.setBounds(29, 85, 96, 21);
		contentPane.add(lblPatientid);

		JLabel lblPatientname = new JLabel("Patient_Name");
		lblPatientname.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblPatientname.setBounds(29, 124, 112, 22);
		contentPane.add(lblPatientname);

		PatientName = new JTextField();
		PatientName.setBounds(201, 117, 116, 22);
		contentPane.add(PatientName);
		PatientName.setColumns(10);

		JLabel lblDocid = new JLabel("doc_id");
		lblDocid.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDocid.setBounds(29, 170, 56, 16);
		contentPane.add(lblDocid);

		JButton btnSearch = new JButton("search");
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					System.out.println("in search");
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
					int id = Integer.parseInt(txtid.getText().trim());

					System.out.println(id);
					java.sql.Statement st = cn.createStatement();
					String query = "select * from Patient where Patient_id = " + id;

					System.out.println(query);

					ResultSet Rs = st.executeQuery(query);
					while (Rs.next()) {
						System.out.println(Rs.getString(1));
						/*
						 * model.setRowCount(0);
						 * 
						 * model.addRow(new Object[]{Rs.getInt(1),Rs.getString(2)});
						 */
						PatientName.setText(Rs.getString(3));
					}

				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} catch (SQLException e2) {
					e2.printStackTrace();
				}
			}
		});

		btnSearch.setBounds(360, 85, 97, 25);
		contentPane.add(btnSearch);

		JComboBox docid = new JComboBox();
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "doctorAppointment",
					"123");
			String query = "select * from doctor";
			java.sql.Statement str = con.createStatement();
			ResultSet rs = str.executeQuery(query);
			while (rs.next()) {
				docid.addItem(rs.getInt(1) + " " + rs.getString(2));
			}

		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		docid.setBounds(201, 164, 116, 22);
		contentPane.add(docid);

		ItemListener DoctorListener = new ItemListener() {
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				System.out.println("in listener");
				System.out.println("doctor: " + itemEvent.getItem());
				String str = (String) itemEvent.getItem();
				str = str.substring(0, 3).trim();
				System.out.println(str);
				int docid = Integer.parseInt(str);
				try {

					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection cn;
					cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
					String str1 = " select Day_avl_from,Day_avl_to,Time_avl_from,time_avl_to from doctor where  doc_id = "
							+ docid;
					System.out.println(str1);
					java.sql.Statement st = cn.createStatement();
					ResultSet rs = st.executeQuery(str1);

					while (rs.next()) {
						System.out.println("in rs");
						Dayavlfrom.setText(rs.getString(1));
						Dayavlto.setText(rs.getString(2));
						timeavlfrom.setText(rs.getString(3));
						timeavlto.setText(rs.getString(4));

					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		docid.addItemListener(DoctorListener);

		JLabel lblApptime = new JLabel("App_Time");
		lblApptime.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblApptime.setBounds(29, 355, 96, 31);
		contentPane.add(lblApptime);

		JLabel lblDayavlfrom = new JLabel("Day_avl_from");
		lblDayavlfrom.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDayavlfrom.setBounds(29, 215, 112, 20);
		contentPane.add(lblDayavlfrom);

		JLabel lblTimeavlfrom = new JLabel("Time_avl_from");
		lblTimeavlfrom.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTimeavlfrom.setBounds(29, 251, 112, 31);
		contentPane.add(lblTimeavlfrom);

		Dayavlfrom = new JTextField();
		Dayavlfrom.setBounds(201, 213, 116, 22);
		contentPane.add(Dayavlfrom);

		Dayavlfrom.setColumns(10);
		JLabel lblDayavlto = new JLabel("Day_avl_to");
		lblDayavlto.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblDayavlto.setBounds(372, 216, 97, 16);
		contentPane.add(lblDayavlto);

		Dayavlto = new JTextField();
		Dayavlto.setBounds(536, 210, 116, 22);
		contentPane.add(Dayavlto);
		Dayavlto.setColumns(10);

		JLabel lblTimeavlto = new JLabel("Time_avl_to");
		lblTimeavlto.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblTimeavlto.setBounds(372, 260, 133, 16);
		contentPane.add(lblTimeavlto);

		timeavlfrom = new JTextField();
		timeavlfrom.setBounds(201, 257, 116, 22);
		contentPane.add(timeavlfrom);
		timeavlfrom.setColumns(10);

		timeavlto = new JTextField();
		timeavlto.setBounds(536, 257, 116, 22);
		contentPane.add(timeavlto);
		timeavlto.setColumns(10);

		txtid = new JTextField();
		txtid.setBounds(201, 86, 116, 22);
		contentPane.add(txtid);
		txtid.setColumns(10);

		JButton btnBook = new JButton("Book");
		btnBook.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnBook.addActionListener(new ActionListener() {
			// private Date dateChooser;

			public void actionPerformed(ActionEvent e) {
				// App_Id=textField.getText().trim();
				System.out.println("in action performed");
				Patient_Id = txtid.getText();
				System.out.println(Patient_Id);
				Patient_Name = PatientName.getText().toString();
				System.out.println(Patient_Name);
				
				doc_Id = docid.getSelectedItem().toString().substring(0,3);
				System.out.println(doc_Id);
				DAY_AVL_FROM = Dayavlfrom.getText();
				System.out.println(DAY_AVL_FROM);
				DAY_AVL_TO = Dayavlto.getText();
				System.out.println(DAY_AVL_TO);
				TIME_AVL_FROM = timeavlfrom.getText();
				System.out.println(TIME_AVL_FROM);
				TIME_AVL_TO = timeavlto.getText();
				System.out.println(TIME_AVL_TO);
				
				System.out.println("App_Date" + dateChooser.getDate());
				App_Date = dateChooser.getDate();
				System.out.println(App_Date);
				
				app_Time = comboBox.getSelectedItem().toString();
				System.out.println(app_Time);
				
				Specialization = cmbspecialization.getSelectedItem().toString();
				System.out.println(Specialization);
				Book();
			}
		});
		btnBook.setBounds(555, 458, 97, 41);
		contentPane.add(btnBook);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReceptionistPage lp = new ReceptionistPage();
				lp.setVisible(true);
				show(false);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 19));
		btnBack.setBounds(726, 458, 97, 42);
		contentPane.add(btnBack);

		comboBox = new JComboBox();
		comboBox.setModel(
				new DefaultComboBoxModel(new String[] { "", "9:00am", "10:00am", "11:00am", "12:00am", "1:00am" }));
		comboBox.setBounds(201, 361, 116, 22);
		contentPane.add(comboBox);

		JLabel lblSpecialization = new JLabel("Specialization");
		lblSpecialization.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblSpecialization.setBounds(29, 405, 112, 25);
		contentPane.add(lblSpecialization);

		 cmbspecialization = new JComboBox();
		cmbspecialization.setModel(new DefaultComboBoxModel(new String[] { "", "Cardialogist", "surgeon", "kidn`+eys",
				"neurologist", "psychologist", "urologist", "nephroilogist" }));
		cmbspecialization.setBounds(201, 408, 116, 22);
		contentPane.add(cmbspecialization);

		JLabel lblAppdate = new JLabel("App_Date");
		lblAppdate.setFont(new Font("Sitka Subheading", Font.PLAIN, 17));
		lblAppdate.setBounds(29, 306, 80, 25);
		contentPane.add(lblAppdate);

		dateChooser.setBounds(201, 309, 116, 22);
		contentPane.add(dateChooser);

	}

	public void Book() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");

			PreparedStatement st = cn
					.prepareStatement("insert into Appointment values (AppointmentSeq.nextval,?,?,?,?,?,?)");

			st.setString(1, Patient_Id);
			st.setString(2, Patient_Name);
			st.setString(3, doc_Id);
			java.sql.Date sqlDate = new java.sql.Date(App_Date.getTime());
			st.setDate(4, sqlDate);
			System.out.println(App_Date);// upto here printing the output
	
			st.setString(5, app_Time);
			System.out.println("app_Time" + app_Time);
			st.setString(6, Specialization);
			System.out.println("specialization" + Specialization);
			int i = st.executeUpdate();
			if (i != 0) {
				JOptionPane.showMessageDialog(null, "appointment Booked sucessfully", "Welcome",
						JOptionPane.PLAIN_MESSAGE);
				show(true);
			} else

				JOptionPane.showMessageDialog(null, "appointment not Booked Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);

		}

		catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}
}
