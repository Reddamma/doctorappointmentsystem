package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.border.EmptyBorder;
import javax.swing.text.JTextComponent;

import net.proteanit.sql.DbUtils;

import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.*;
import javax.swing.*;

public class UpdateDoctor extends JFrame {
	String DOC_NAME, PHONE_NO, SPEC_ID, PASSWORD, DAY_AVL_FROM, DAY_AVL_TO, TIME_AVL_FROM, TIME_AVL_TO;

	private JPanel contentPane;
	private JTextField txtdocname1;

	private JTable table;
	private JComboBox comboBox;

	private Container txtTimeTo;

	private JTextComponent lblNewLabel;
	private JTextField txtdocname;
	private JTextField txtPhNo;

	private JTextField txtavlfrom;
	private JTextComponent txtName;
	private JTextField txtavlto;
	private JPasswordField passwordField;
	private JTextComponent txtPhoneno;
	private JTextComponent txtSpecid;
	private JTextComponent cmbavlfrom;
	private JComboBox cmbAvlFrom;
	private JComboBox cmbAvlTo;
	private JTextField avlfrom;
	private JTextField avlto;

	private JTextComponent dayavl;

	private JTextComponent Dayavlto;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateDoctor frame = new UpdateDoctor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// Connection connection=null;
	public void refreshTable() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "doctorAppointment",
					"123");
			String query = "select * from doctor";
			PreparedStatement pst = con.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			table.setModel(DbUtils.resultSetToTableModel(rs));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public void fillComboBox() {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "doctorAppointment",
					"123");
			String query = "select * from doctor";
			PreparedStatement pst = con.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			// table.setModel(DbUtils.resultSetToTableModel(rs));

			while (rs.next()) {
				comboBox.addItem(rs.getString("DOC_NAME"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void RefreshComboBox() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "doctorAppointment",
					"123");
			String query = "select * from doctor where doc_name=? ";
			PreparedStatement pst = con.prepareStatement(query);
			pst.setString(1, (String) comboBox.getSelectedItem());
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				txtdocname1.setText(rs.getString("DOC_NAME"));
				txtPhNo.setText(rs.getString("PHONE_NO"));
				txtSpecid.setText(rs.getString("SPEC_ID"));
				passwordField.setText(rs.getString("PASSWORD"));
				avlfrom.setText(rs.getString("DAY_AVL_FROM"));
				avlto.setText(rs.getString("DAY_AVL_To"));
				txtavlfrom.setText(rs.getString("TIME_AVL_FROM"));
				txtavlto.setText(rs.getString("TIME_AVL_to"));
			}
			pst.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame.
	 */
	public UpdateDoctor() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 838, 485);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(266, 79, 542, 346);
		contentPane.add(scrollPane);

		table = new JTable();
		scrollPane.setViewportView(table);

		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {

			private JTextComponent txtPhoneno;
			private JTextComponent txtSpecid;
			private Component Password;
			private JTextComponent Dayavlto;

			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",
							"doctorAppointment", "123");
					String query = "Update Doctor set DOC_NAME ='" + txtdocname1.getText()+"'"/*+ "',PHONE_NO='"
							+ txtPhNo.getText() + "',SPEC_ID='" + txtSpecid.getText() + "',PASSWORD='"
							+ passwordField.getText() + "',DAY_AVL_FROM='" + avlfrom.getText() + "',DAY_AVL_TO='"
							+ avlto.getText() + "',TIME_AVL_FROM='" + txtavlfrom.getText() + "',TIME_AVL_TO='"
							+ txtavlto.getText() + "' where DOC_NAME ='" + txtName.getText() + "*/;
					PreparedStatement pst = con.prepareStatement(query);
					pst.execute();
					JOptionPane.showMessageDialog(null, "Data Updated");
				} catch (ClassNotFoundException e2) {
					e2.printStackTrace();
				} catch (SQLException e3) {
					e3.printStackTrace();
				} catch (NullPointerException e3) {
					e3.printStackTrace();
				}
				refreshTable();
				txtdocname1.setText(" ");
				refreshTable();
				fillComboBox();
				RefreshComboBox();
			}
		});

		btnUpdate.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnUpdate.setBounds(161, 393, 89, 32);
		contentPane.add(btnUpdate);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage adminhome = new AdminPage();
				adminhome.show();
				show(false);
			}
		});

		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnCancel.setBounds(12, 393, 89, 32);
		contentPane.add(btnCancel);

		JLabel lblDocname = new JLabel("doc_Name");
		lblDocname.setBounds(12, 119, 79, 16);
		contentPane.add(lblDocname);
		txtdocname1 = new JTextField();
		txtdocname1.setBounds(116, 116, 116, 22);
		contentPane.add(txtdocname1);
		txtdocname1.setColumns(10);

		JLabel lblPhoneno = new JLabel("Phone_No");
		lblPhoneno.setBounds(12, 154, 79, 16);
		contentPane.add(lblPhoneno);
		txtPhNo = new JTextField();
		txtPhNo.setBounds(116, 151, 116, 22);
		contentPane.add(txtPhNo);
		txtPhNo.setColumns(10);

		JLabel lblSpecid = new JLabel("Spec_ID");
		lblSpecid.setBounds(12, 184, 79, 27);
		contentPane.add(lblSpecid);

		txtSpecid = new JTextField();
		txtSpecid.setBounds(116, 186, 116, 22);
		contentPane.add(txtSpecid);
		((JTextField) txtSpecid).setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(12, 224, 56, 16);
		contentPane.add(lblPassword);

		JLabel dayavl = new JLabel("Day_avl_from");
		dayavl.setBounds(12, 259, 89, 16);
		contentPane.add(dayavl);

		JLabel Dayavlto = new JLabel("Day_avl_to");
		Dayavlto.setBounds(12, 288, 79, 16);
		contentPane.add(Dayavlto);

		JLabel lblTimeavlfrom = new JLabel("Time_avl_from");
		lblTimeavlfrom.setBounds(12, 317, 101, 16);
		contentPane.add(lblTimeavlfrom);

		txtavlfrom = new JTextField();
		txtavlfrom.setBounds(116, 314, 116, 22);
		contentPane.add(txtavlfrom);
		txtavlfrom.setColumns(10);

		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",
							"doctorAppointment", "123");
					String query = "select * from doctor where Doc_name=? ";
					PreparedStatement pst = con.prepareStatement(query);
					pst.setString(1, (String) comboBox.getSelectedItem());
					ResultSet rs = pst.executeQuery();
					while (rs.next()) {

						txtdocname1.setText(rs.getString("DOC_NAME"));
						txtPhNo.setText(rs.getString("PHONE_NO"));
						txtSpecid.setText(rs.getString("SPEC_ID"));
						passwordField.setText(rs.getString("PASSWORD"));
						avlfrom.setText(rs.getString("DAY_AVL_FROM"));
						avlto.setText(rs.getString("DAY_AVL_To"));
						txtavlfrom.setText(rs.getString("TIME_AVL_FROM"));
						txtavlto.setText(rs.getString("TIME_AVL_to"));
					}
					pst.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		comboBox.setBounds(116, 79, 116, 24);
		contentPane.add(comboBox);

		JButton btnLoadDeptData = new JButton("Load Dept Data");
		btnLoadDeptData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Class.forName("oracle.jdbc.driver.OracleDriver");
					Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe",
							"doctorAppointment", "123");
					String query = "select * from doctor";
					PreparedStatement pst = con.prepareStatement(query);
					ResultSet rs = pst.executeQuery();
					table.setModel(DbUtils.resultSetToTableModel(rs));
				} catch (Exception e) {
					e.printStackTrace();
				}
				refreshTable();
				fillComboBox();
				RefreshComboBox();
			}
		});
		btnLoadDeptData.setBounds(266, 36, 542, 32);
		contentPane.add(btnLoadDeptData);
		JLabel lblSelectData = new JLabel("select Data");
		lblSelectData.setBounds(12, 79, 79, 16);
		contentPane.add(lblSelectData);

		JLabel lblTimeavlto = new JLabel("Time_avl_to");
		lblTimeavlto.setBounds(12, 346, 89, 16);
		contentPane.add(lblTimeavlto);

		txtavlto = new JTextField();
		txtavlto.setBounds(116, 349, 116, 22);
		contentPane.add(txtavlto);
		txtavlto.setColumns(10);

		passwordField = new JPasswordField();
		passwordField.setBounds(116, 221, 115, 22);
		contentPane.add(passwordField);

		avlfrom = new JTextField();
		avlfrom.setBounds(116, 256, 116, 22);
		contentPane.add(avlfrom);
		avlfrom.setColumns(10);

		avlto = new JTextField();
		avlto.setBounds(116, 285, 116, 22);
		contentPane.add(avlto);
		avlto.setColumns(10);
	}
}