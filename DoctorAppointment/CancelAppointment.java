package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class CancelAppointment<cmbEnter> extends JFrame {

	private JPanel contentPane;
	String Specialization;
	//protected JComboBox cmbSpecialization;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		CancelAppointment frame = new CancelAppointment();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public CancelAppointment() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 621, 387);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDeleteDoctorName = new JLabel("Cancel Appointment_Id");
		lblDeleteDoctorName.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDeleteDoctorName.setBounds(220, 30, 263, 31);
		contentPane.add(lblDeleteDoctorName);

		JLabel lblEnterDoctorName = new JLabel("Enter App_Id :");
		lblEnterDoctorName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEnterDoctorName.setBounds(107, 104, 110, 16);
		contentPane.add(lblEnterDoctorName);

		JComboBox cmbspecialization = new JComboBox();
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			String str = "select * from Appointment order by Specialization";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);
			while (rs.next()) {
				cmbspecialization.addItem(rs.getString(1) + "    " + rs.getString(2));

			}
		} catch (Exception e) {

		}
		cmbspecialization.setBounds(277, 102, 181, 22);
		contentPane.add(cmbspecialization);

		JButton btnDelete = new JButton("Cancel");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Specialization = cmbspecialization.getSelectedItem().toString().substring(0,3);
				int AppId = Integer.parseInt(Specialization);
				delete(AppId);
			}
		});

		btnDelete.setBounds(466, 223, 97, 25);
		contentPane.add(btnDelete);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			   DoctorPage ap = new DoctorPage();
				ap.setVisible(true);
				show(false);
			}
		});
		btnBack.setBounds(66, 223, 97, 25);
		contentPane.add(btnBack);
	}

	public void delete(int AppId) {
		Connection cn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			stmt = cn.createStatement();
			System.out.println("DELETE from Appointment where APP_ID= " + AppId);
			boolean i = stmt.execute("DELETE from Appointment where APP_ID= " + AppId);
			if (i == false) {
				JOptionPane.showMessageDialog(null, "Cancelled sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
				show(true);
			} else
				JOptionPane.showMessageDialog(null, "Cancelled Not  Successfully", "Error",	JOptionPane.ERROR_MESSAGE);

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {

			e.printStackTrace();
		}

	}
}
