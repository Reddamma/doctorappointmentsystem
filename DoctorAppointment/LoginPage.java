package DoctorAppointment;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class LoginPage extends JFrame {

	private JPanel contentPane;
	private JTextField txtAdmin_1;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		LoginPage frame = new LoginPage();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public LoginPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 300, 872, 620);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblUsername.setBounds(99, 161, 136, 49);
		contentPane.add(lblUsername);

		txtAdmin_1 = new JTextField();
		txtAdmin_1.setToolTipText("admin");
		txtAdmin_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtAdmin_1.setBounds(255, 167, 184, 42);
		contentPane.add(txtAdmin_1);
		txtAdmin_1.setColumns(10);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPassword.setBounds(99, 241, 149, 49);
		contentPane.add(lblPassword);

		JButton btnLogin = new JButton("login");

		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnLogin.setBounds(599, 355, 136, 42);
		contentPane.add(btnLogin);

		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 20));
		passwordField.setToolTipText("1234");
		passwordField.setBounds(255, 247, 184, 42);
		contentPane.add(passwordField);

		JLabel lblDoctorAppointment = new JLabel("Doctor Appointment");
		lblDoctorAppointment.setFont(new Font("Tahoma", Font.PLAIN, 31));
		lblDoctorAppointment.setBounds(237, 30, 329, 68);
		contentPane.add(lblDoctorAppointment);

		JList list = new JList();
		list.setBounds(200, 396, 1, 1);
		contentPane.add(list);
		JRadioButton rdbtnAdmin = new JRadioButton("Admin");
		rdbtnAdmin.setSelected(true);
		rdbtnAdmin.setFont(new Font("Tahoma", Font.PLAIN, 27));
		rdbtnAdmin.setBounds(84, 461, 117, 42);
		contentPane.add(rdbtnAdmin);

		JRadioButton rdbtnDoctor = new JRadioButton("Doctor");
		rdbtnDoctor.setFont(new Font("Tahoma", Font.PLAIN, 27));
		rdbtnDoctor.setBounds(333, 466, 127, 33);
		contentPane.add(rdbtnDoctor);
		JRadioButton rdbtnReceptionist = new JRadioButton("Receptionist");

		rdbtnReceptionist.setFont(new Font("Tahoma", Font.PLAIN, 27));
		rdbtnReceptionist.setBounds(584, 459, 173, 47);
		contentPane.add(rdbtnReceptionist);
		ButtonGroup g = new ButtonGroup();
		g.add(rdbtnAdmin);
		g.add(rdbtnDoctor);
		g.add(rdbtnReceptionist);

		btnLogin.addActionListener(new ActionListener() {

			private String Uname;
			private String Upass;
			private String Upsw;

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {

				String Uname = txtAdmin_1.getText();
				String Upass = passwordField.getText();
				if (rdbtnAdmin.isSelected()) // admin
				{
					if (Uname.equals("admin") && Upass.equals("1234")) {
						JOptionPane.showMessageDialog(null, "login successful", "admin", JOptionPane.PLAIN_MESSAGE);
						AdminPage I = new AdminPage();
						I.setVisible(true);
						show(false);

					} else {
						JOptionPane.showMessageDialog(null, "Incorrect login or password", "Warning",
								JOptionPane.WARNING_MESSAGE);

					}
				} else if (rdbtnDoctor.isSelected()) // Doctor
				{
					try {

						Class.forName("oracle.jdbc.driver.OracleDriver");
						Connection con;
						con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment",
								"123");
						java.sql.Statement st = con.createStatement();
						ResultSet r = st.executeQuery(
								"select * from Doctors where uname = '" + Uname + "' and pws = '" + Upass + "'");
						if (r.next()) {
							JOptionPane.showMessageDialog(null, "login successful", "Doctor",
									JOptionPane.PLAIN_MESSAGE);
							DoctorPage Dp = new DoctorPage();
							Dp.setVisible(true);
							show(false);
						} else
							JOptionPane.showMessageDialog(null, "Incorrect login or password", "Warning",
									JOptionPane.WARNING_MESSAGE);

					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}

				} else if (rdbtnReceptionist.isSelected()) // Receptionist
				{

					try {

						Class.forName("oracle.jdbc.driver.OracleDriver");
						Connection con;
						con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment",
								"123");
						java.sql.Statement st = con.createStatement();
						ResultSet r = st.executeQuery(
								"select * from Reception where uname = '" + Uname + "' and upass = '" + Upass + "'");
						if (r.next()) {
							JOptionPane.showMessageDialog(null, "Incorrect login or password", "Warning",
									JOptionPane.WARNING_MESSAGE);

						} else {

							JOptionPane.showMessageDialog(null, "login successful", "Receptionist",
									JOptionPane.PLAIN_MESSAGE);
							ReceptionistPage Rp = new ReceptionistPage();
							Rp.setVisible(true);
							show(false);

						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

			}

		});
	}

}
