package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class AddPatient extends JFrame {

	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtphno;
	protected String Patient_ID;
	protected String Patient_Name;
	protected String Phone_No;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddPatient frame = new AddPatient();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddPatient() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 799, 578);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label = new JLabel("");
		label.setBounds(187, 30, 56, 16);
		contentPane.add(label);

		JLabel lblPhoneno = new JLabel("Phone_No     :");
		lblPhoneno.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblPhoneno.setBounds(75, 155, 123, 34);
		contentPane.add(lblPhoneno);

		txtphno = new JTextField();
		txtphno.setBounds(315, 155, 250, 36);
		contentPane.add(txtphno);
		txtphno.setColumns(10);

		JLabel lblName = new JLabel("Patient_Name:");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblName.setBounds(75, 214, 150, 34);
		contentPane.add(lblName);

		txtName = new JTextField();
		txtName.setBounds(315, 214, 250, 36);
		contentPane.add(txtName);
		txtName.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Patient_Name = txtName.getText().trim();
				Phone_No = txtphno.getText().trim();
				insert();
			}
		});
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnAdd.setBounds(578, 351, 97, 42);
		contentPane.add(btnAdd);

		JLabel lblAddpatient = new JLabel("AddPatient");
		lblAddpatient.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblAddpatient.setBounds(217, 30, 206, 64);
		contentPane.add(lblAddpatient);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReceptionistPage ap = new ReceptionistPage();
				ap.setVisible(true);
				show(false);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnBack.setBounds(126, 351, 97, 42);
		contentPane.add(btnBack);
	}

	public void insert() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			PreparedStatement st = cn.prepareStatement("insert into Patient values (PatientSeq.nextval,?,?)");

			st.setString(1, Phone_No);
			st.setString(2, Patient_Name);

			int i = st.executeUpdate();
			if (i != 0) {
				JOptionPane.showMessageDialog(null, "Patient added sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
			} else
				JOptionPane.showMessageDialog(null, "Patient Not added Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {

			e.printStackTrace();
			System.out.println(e.getMessage());

		} catch (NullPointerException e) {

			e.printStackTrace();
		}
	}
}