package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DoctorPage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DoctorPage frame = new DoctorPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DoctorPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 806, 537);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnDoctorpage = new JMenu("Doctorpage");
		mnDoctorpage.setFont(new Font("Sitka Subheading", Font.BOLD, 25));
		menuBar.add(mnDoctorpage);

		JMenuItem mntmView = new JMenuItem("View");
		mntmView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewAppointment ud = new ViewAppointment();
				ud.setVisible(true);
				show(false);
			}
		});
		mntmView.setFont(new Font("Segoe UI", Font.BOLD, 16));
		mnDoctorpage.add(mntmView);

		JMenuItem mntmCancel = new JMenuItem("cancel");
		mntmCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CancelAppointment ud = new CancelAppointment();
				ud.setVisible(true);
				show(false);
			}
		});
		mntmCancel.setFont(new Font("Segoe UI", Font.BOLD, 16));
		mnDoctorpage.add(mntmCancel);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
	}
}
