package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class AdminPage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		AdminPage frame = new AdminPage();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public AdminPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 823, 591);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnDoctor = new JMenu("Doctor");
		mnDoctor.setFont(new Font("Sitka Subheading", Font.BOLD, 26));
		menuBar.add(mnDoctor);

		JMenuItem mntmAdd = new JMenuItem("Add");
		mntmAdd.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DoctorAdd ad = new DoctorAdd();
				ad.setVisible(true);
				show(false);
			}
		});
		mnDoctor.add(mntmAdd);

		JMenuItem mntmDelete = new JMenuItem("Delete");
		mntmDelete.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DoctorDelete dd = new DoctorDelete();
				dd.setVisible(true);
				show(false);

			}
		});
		mnDoctor.add(mntmDelete);

		JMenuItem mntmUpdate = new JMenuItem("Update");
		mntmUpdate.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				UpdateDoctor ud = new UpdateDoctor();
				ud.setVisible(true);
				show(false);
			}
		});
		mnDoctor.add(mntmUpdate);

		JMenu mnView = new JMenu("View");
		mnView.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewDoctor ud = new ViewDoctor();
				ud.setVisible(true);
				show(false);
			}
		});

		mnDoctor.add(mnView);

		JMenuItem mntmSearch = new JMenuItem("Search");
		mntmSearch.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ViewDoctor ud = new ViewDoctor();
				ud.setVisible(true);
				show(false);

			}
		});
		mnView.add(mntmSearch);

		JMenu mnReceptionist = new JMenu("Receptionist");
		mnReceptionist.setFont(new Font("Sitka Subheading", Font.BOLD, 26));
		menuBar.add(mnReceptionist);

		JMenuItem mntmAdd_1 = new JMenuItem("Add");
		mntmAdd_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmAdd_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReceptionistAdd rd = new ReceptionistAdd();
				rd.setVisible(true);
				show(false);
			}
		});

		mnReceptionist.add(mntmAdd_1);

		JMenuItem mntmDelete_1 = new JMenuItem("Delete");
		mntmDelete_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mnReceptionist.add(mntmDelete_1);

		JMenuItem mntmUpdate_1 = new JMenuItem("Update");
		mntmUpdate_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmUpdate_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ReceptionistUpdate rd = new ReceptionistUpdate();
				rd.setVisible(true);
				show(false);
			}
		});
		mnReceptionist.add(mntmUpdate_1);

		JMenu mnView_1 = new JMenu("View");
		mnView_1.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mnView_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewReceptionist ud = new ViewReceptionist();
				ud.setVisible(true);
				show(false);
			}
		});
		mnReceptionist.add(mnView_1);

		JMenuItem mntmSearch_2 = new JMenuItem("Search");
		mntmSearch_2.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mntmSearch_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewReceptionist ud = new ViewReceptionist();
				ud.setVisible(true);
				show(false);
			}
		});
		mnView_1.add(mntmSearch_2);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 23));
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginPage lp = new LoginPage();
				lp.setVisible(true);
				show(false);
			}
		});
		btnBack.setBounds(459, 310, 97, 45);
		getContentPane().add(btnBack);
		getContentPane().setLayout(null);
		JList list = new JList();
		list.setBounds(44, 108, -12, -19);
		contentPane.add(list);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());

			}
		});
	}
}
