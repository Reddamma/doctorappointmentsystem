package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class ReceptionistAdd extends JFrame {
	String Rec_Name, Password, Phone_No;
	private JPanel contentPane;
	private JTextField txtName;
	private JTextField txtphn;
	protected JLabel txtpsw;
	private JPasswordField pswd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		ReceptionistAdd frame = new ReceptionistAdd();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public ReceptionistAdd() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Rec_Name:");
		lblNewLabel.setBounds(39, 77, 111, 16);
		contentPane.add(lblNewLabel);

		txtName = new JTextField();
		txtName.setBounds(171, 74, 116, 22);
		contentPane.add(txtName);
		txtName.setColumns(10);

		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(38, 172, 95, 16);
		contentPane.add(lblPassword);

		JLabel lblPhoneNo = new JLabel("Phone_No:");
		lblPhoneNo.setBounds(39, 124, 80, 16);
		contentPane.add(lblPhoneNo);

		txtphn = new JTextField();
		txtphn.setBounds(171, 121, 116, 22);
		contentPane.add(txtphn);
		txtphn.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rec_Name = txtName.getText();
				Password = pswd.getText();
				Phone_No = txtphn.getText();
				insert();
			}
		});
		btnAdd.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnAdd.setBounds(305, 242, 97, 25);
		contentPane.add(btnAdd);

		pswd = new JPasswordField();
		pswd.setBounds(171, 169, 116, 22);
		contentPane.add(pswd);

		JLabel lblAddReceiptionist = new JLabel("Add Receiptionist");
		lblAddReceiptionist.setFont(new Font("Sitka Subheading", Font.BOLD, 19));
		lblAddReceiptionist.setBounds(171, 13, 262, 36);
		contentPane.add(lblAddReceiptionist);

		JButton btnNewButton = new JButton("Back");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage lp = new AdminPage();
				lp.setVisible(true);
				show(false);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton.setBounds(445, 242, 97, 25);
		contentPane.add(btnNewButton);

	}

	public void insert() {

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");

			PreparedStatement st = cn
					.prepareStatement("insert into Receptionist values (ReceptionistSeq.nextval,?,?,?)");
			st.setString(1, Rec_Name);
			st.setString(2, Phone_No);
			st.setString(3, Password);
			int i = st.executeUpdate();
			if (i != 0) {
				JOptionPane.showMessageDialog(null, "Receptionist added sucessfully", "Welcome",
						JOptionPane.PLAIN_MESSAGE);
			} else
				JOptionPane.showMessageDialog(null, "Receptionist Not added Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {

			e.printStackTrace();
		}
	}
}
