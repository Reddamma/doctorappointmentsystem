package DoctorAppointment;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.Collections;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ViewAppointment extends JFrame {
   
DefaultTableModel model = new DefaultTableModel();
    Container cnt = this.getContentPane();
    JTable jtbl = new JTable(model);
   private Container contentPane;
    JTextField txtSear = new JTextField(); 
      
   
    
public  ViewAppointment() {
	cnt.setLayout(new FlowLayout(FlowLayout.LEFT));
    model.addColumn("App_ID");
    model.addColumn("PATIENT_ID");
    model.addColumn("PATIENT_NAME");
    model.addColumn("DOC_ID");
    model.addColumn("APP_DATE");
    model.addColumn("APP_TIME");
    model.addColumn("SPECIALIZATION");
        JButton btnBack = new JButton("Back");
        btnBack.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
        	DoctorPage lp=new DoctorPage();
			lp.setVisible(true);
			show(false);

        }
        });
        btnBack.setFont(new Font("Sitka Subheading", Font.BOLD, 20));
       btnBack.setBounds(335, 194, 89, 22);
        cnt.add(btnBack);


        try {
        	Class.forName("oracle.jdbc.driver.OracleDriver");
        	Connection cn;
			   cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","DoctorAppointment","123");
            PreparedStatement pstm = cn.prepareStatement("SELECT * from Appointment");
            ResultSet Rs = pstm.executeQuery();
            while(Rs.next()){
                model.addRow(new Object[]{Rs.getInt(1),Rs.getInt(2),Rs.getString(3),Rs.getInt(4),Rs.getString(5),Rs.getString(6),Rs.getString(6)});
            }
        } catch (ClassNotFoundException e) {
            
            e.printStackTrace();
        }catch (SQLException e) {
            
            e.printStackTrace();
        }
         
        JButton btnSearch = new JButton("Search");
        btnSearch.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
        try {
               System.out.println("in search");
               Class.forName("oracle.jdbc.driver.OracleDriver");
               Connection cn;
			   cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","DoctorAppointment","123");
                   String str = txtSear.getText().trim();
                   System.out.println(str);
                   Statement  st = cn.createStatement();
                   String query  = "select * from Appointment where App_Id like '%"+str+"%'" ; 
                   
                   System.out.println(query);

                    ResultSet Rs = st.executeQuery(query);
                    while(Rs.next()==true)
                    {
                   	System.out.println(Rs.getString(1));
                   	model.setRowCount(0);

                   	model.addRow(new Object[]{Rs.getInt("App_ID"),Rs.getInt("PATIENT_ID"),Rs.getString("PATIENT_NAME"),Rs.getInt("DOC_ID"),Rs.getString("APP_DATE"),Rs.getString("APP_TIME"),Rs.getString("SPECIALIZATION")});
                    }
                   
        }
        catch(Exception e)
        {
        e.printStackTrace();
        }

        }
        });
       JLabel lblappid = new JLabel("App_ID:");
       lblappid.setFont(new Font("Tahoma", Font.PLAIN, 18));
        getContentPane().add(lblappid);
        
        getContentPane().add(txtSear);
        txtSear.setColumns(10);
        btnSearch.setFont(new Font("Tahoma", Font.BOLD, 20));
        getContentPane().add(btnSearch);
        
       
        
        JScrollPane pg = new JScrollPane(jtbl);
        cnt.add(pg);
        
       
        this.pack();
    }
            public static void main(String[] args) {
                JFrame fr =new ViewAppointment();
                fr.setTitle("Appointment Details");
                fr.setSize(500,400);
                fr.setLocationRelativeTo(null);
                fr.setVisible(true);
                fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            }

        }


