package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Font;
import java.awt.Window;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;

public class ReceptionistPage extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReceptionistPage frame = new ReceptionistPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ReceptionistPage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 807, 580);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnPatient = new JMenu("Patient");
		mnPatient.setFont(new Font("Sitka Subheading", Font.BOLD, 28));
		menuBar.add(mnPatient);

		JMenuItem mntmAdd = new JMenuItem("Add");
		mntmAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddPatient rd = new AddPatient();
				((Window) rd).setVisible(true);
				show(false);
			}
		});
		mntmAdd.setFont(new Font("Segoe UI", Font.BOLD, 17));
		mnPatient.add(mntmAdd);

		JMenuItem mntmView_1 = new JMenuItem("view");
		mntmView_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewPatient vd = new ViewPatient();
				vd.setVisible(true);
				show(false);
			}
		});
		mntmView_1.setFont(new Font("Segoe UI", Font.BOLD, 20));
		mnPatient.add(mntmView_1);

		JMenu mnAppointment = new JMenu("Appointment");
		mnAppointment.setFont(new Font("Sitka Subheading", Font.BOLD, 28));
		menuBar.add(mnAppointment);

		JMenuItem mntmBook = new JMenuItem("Book");
		mntmBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BookAppointment frame = new BookAppointment();
				frame.setVisible(true);
				show(false);
			}
		});
		mntmBook.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mnAppointment.add(mntmBook);
		
		JMenu mnView = new JMenu("View");
		mnView.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		mnAppointment.add(mnView);
		
		JMenuItem mntmSearch = new JMenuItem("Search");
		mntmSearch.setFont(new Font("Segoe UI", Font.BOLD, 18));
		mnView.add(mntmSearch);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LoginPage lp = new LoginPage();
				lp.setVisible(true);
				show(false);
			}
		});
		btnBack.setFont(new Font("Tahoma", Font.BOLD, 17));
		btnBack.setBounds(564, 342, 97, 42);
		contentPane.add(btnBack);
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());

			}
		});
	}

}
