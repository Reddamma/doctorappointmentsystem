package DoctorAppointment;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DoctorDelete<cmbEnter> extends JFrame {

	private JPanel contentPane;
	String DOC_NAME;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		DoctorDelete frame = new DoctorDelete();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public DoctorDelete() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 621, 387);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblDeleteDoctorName = new JLabel("Delete DocId");
		lblDeleteDoctorName.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblDeleteDoctorName.setBounds(220, 30, 216, 16);
		contentPane.add(lblDeleteDoctorName);

		JLabel lblEnterDoctorName = new JLabel("Enter Doc_ID :");
		lblEnterDoctorName.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblEnterDoctorName.setBounds(107, 104, 110, 16);
		contentPane.add(lblEnterDoctorName);

		JComboBox cmbDocName = new JComboBox();
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection cn;
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			String str = "select * from doctor order by doc_name";
			java.sql.Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(str);
			while (rs.next()) {
				cmbDocName.addItem(rs.getString(1) + "    " + rs.getString(2));

			}
		} catch (Exception e) {

		}
		cmbDocName.setBounds(277, 102, 181, 22);
		contentPane.add(cmbDocName);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DOC_NAME = cmbDocName.getSelectedItem().toString().substring(0, 3);
				int docId = Integer.parseInt(DOC_NAME);
				delete(docId);
			}
		});

		btnDelete.setBounds(466, 223, 97, 25);
		contentPane.add(btnDelete);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdminPage ap = new AdminPage();
				ap.setVisible(true);
				show(false);
			}
		});
		btnBack.setBounds(66, 223, 97, 25);
		contentPane.add(btnBack);
	}

	public void delete(int docId) {
		Connection cn = null;
		Statement stmt = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			cn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "DoctorAppointment", "123");
			stmt = cn.createStatement();
			System.out.println("DELETE from doctor where DOC_id= " + docId);
			boolean i = stmt.execute("DELETE from doctor where DOC_id= " + docId);
			if (i == false) {
				JOptionPane.showMessageDialog(null, "Doctor deleted sucessfully", "Welcome", JOptionPane.PLAIN_MESSAGE);
				show(true);
			} else
				JOptionPane.showMessageDialog(null, "Doctor Not deleted Successfully", "Error",
						JOptionPane.ERROR_MESSAGE);

		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NullPointerException e) {

			e.printStackTrace();
		}

	}
}
